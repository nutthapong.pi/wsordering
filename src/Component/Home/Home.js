import React, { Component } from "react";
import { Navbar, Form, Button } from "react-bootstrap";
import history from "../../history";

export default class Home extends Component {
  render() {
    return (
      <div>
        <Navbar className="bg-light justify-content-between">
          <Form inline>
            <Navbar.Brand >Home</Navbar.Brand>
          </Form>
            <Button onClick ={() => history.push('/')} >Logout</Button>
        </Navbar>
      </div>
    );
  }
}
