import React, { Component } from "react";
import { Button, Form, Container } from "react-bootstrap";
import history from "../../history";

class Login extends Component {
  constructor(props) {
    super(props)

    this.state = {
      username: '',
      password: '',
      currentUser: null,
      message: ''
    }
  }


  onChange = e => {
    const { name, value } = e.target

    this.setState({
      [name]: value
    })
  }

  onSubmit = (e) => {

    const { username, password } = this.state;

    if(username === "admin" && password === "artrazer1")
    {
      history.push("/Home");
    } 
    else{
      this.state = {
        username: '',
        password: '',
      }
    }
  };

  render() {
    return (
      <div>
        {
           <Container>
          <Form style={{ margin: 100 }} onSubmit={this.onSubmit}>
            <Form.Group controlId="formBasicEmail">
              <Form.Label>Username</Form.Label>
              <Form.Control type="text" name="username" placeholder="Enter username" onChange={this.onChange} />
            </Form.Group>

            <Form.Group controlId="formBasicPassword">
              <Form.Label>Password</Form.Label>
              <Form.Control type="password" name="password" placeholder="Password" onChange={this.onChange} />
            </Form.Group>
            <Button type="submit" variant="primary" >
              Submit
            </Button>
          </Form>
        </Container> 
        }
      </div>
    );
  }
}

export default Login;
