import React, { Component } from 'react'
import { Router, Switch, Route } from "react-router-dom";

import Login from './Component/Login/Login'
import Home from './Component/Home/Home'
import history from './history'

export default class Routers extends Component {
    render() {
        return (
            <Router history={history}>
                <Switch>
                    <Route exact path="/"  component={Login}/>
                    <Route path="/Home" component={Home}/>
                    <Route  component={Login}/>
                  
                </Switch>
            </Router>
        )
    }
}
